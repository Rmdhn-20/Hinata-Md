const fs = require('fs')
const chalk = require('chalk')

// Free Apikey
global.APIs = {
	riy: 'https://api-xriy04.herokuapp.com',
}

// Other
global.owner = ['628889616144']
global.premium = ['628889616144']
global.youtube = 'https://bit.ly/3d4GkKI'
global.ownername = 'ExZ'
global.botname ='Hinata-Botz'
global.donasi = {
    saweria: 'https://saweria.co/Ekuzika',
    nomor: '08889616144'
}
global.packname = 'Hinata-Botz'
global.author = '© Hinata-Botz\nPowered by ⌜ WhatsApp ⌟'
global.sessionName = 'session'
global.prefa = ['','!','.','🐦','🐤','🗿']
global.sp = '⭔'
global.mess = {
    success: '_[ ✓ ] Success_',
    admin: 'Fitur Khusus Admin Group!',
    botAdmin: 'Bot Harus Menjadi Admin Terlebih Dahulu!',
    owner: 'Fitur Khusus Owner Bot',
    group: 'Fitur Digunakan Hanya Untuk Group!',
    private: 'Fitur Digunakan Hanya Untuk Private Chat!',
    bot: 'Fitur Khusus Pengguna Nomor Bot',
    wait: '*[  !  ] Loading...*'
}
global.thumb = fs.readFileSync('./image/hinata.jpg')
global.vn = './sound/menu.mp3'

let file = require.resolve(__filename)
fs.watchFile(file, () => {
	fs.unwatchFile(file)
	console.log(chalk.redBright(`Update'${__filename}'`))
	delete require.cache[file]
	require(file)
})
